package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

type Attribute struct {
	Name  string `json:"Name"`
	Value string `json:"Value"`
}
type Item struct {
	Name       string      `json:"Name"`
	Attributes []Attribute `json:"Attributes"`
}

type Quips struct {
	Items []Item `json:"Items"`
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	re := regexp.MustCompile("\n$")
	f, err := os.Open("sdb-quips.json")
	check(err)
	defer f.Close()

	data, err := ioutil.ReadAll(f)
	check(err)
	defer f.Close()

	var quips Quips
	err = json.Unmarshal(data, &quips)
	check(err)

	for _, i := range quips.Items {
		for _, a := range i.Attributes {
			fmt.Println(`{ "create": {} }`)
			quip := strings.ReplaceAll(re.ReplaceAllString(a.Value, ""), "\n", "\\n");
			fmt.Printf("{ \"quip\":\"%s\"}\n", strings.ReplaceAll(quip, "\"", ""))
		}
	}

}
